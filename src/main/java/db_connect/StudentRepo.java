package db_connect;

import model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentRepo {
    private DbConnector dbConnector;
    
    public StudentRepo() {
        dbConnector = new DbConnector();
    }
    
    
    public boolean addStudent(Student student) {
        String sql = "INSERT INTO student (name,age,sex,phone) VALUES ('李四','18','女','15567879876');";
        PreparedStatement pstmt;

        try {
            pstmt = dbConnector.createConnect().prepareStatement(sql);
            pstmt.execute();
            pstmt.close();
            return true;
        } catch (SQLException e) {
            System.out.println("Add failed.");
        }
        return false;
    }
    
    public List<Student> findStudents() {
        String sql = "select * from student";
        List<Student> students = new ArrayList<>();
        PreparedStatement pstmt;

        try {
            pstmt = dbConnector.createConnect().prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery(sql);
            while(rs.next()){
                Student s = new Student (rs.getInt("id"), rs.getString("name"),
                        rs.getInt("age"), rs.getString("sex"),
                        rs.getString("phone"));
                students.add(s);
            }

        } catch (SQLException e) {
            System.out.println("Search failed.");
        }

        return students;
    }
    
    public boolean updateStudent(Student student) {
        String sql = "UPDATE student SET name = ?, age = ?, sex = ?, phone = ? WHERE id = ?";
        PreparedStatement pstmt;

        try {
            pstmt = dbConnector.createConnect().prepareStatement(sql);

            pstmt.setString(1, student.getName());
            pstmt.setInt(2, student.getAge());
            pstmt.setString(3, student.getGender());
            pstmt.setString(4, student.getPhone());
            pstmt.setInt(5, student.getId());

            pstmt.execute();
            pstmt.close();
            return true;
        } catch (SQLException e) {
            System.out.println("Updating student information failed.");
            System.out.println(e.toString());
        }

        return false;
    }
    
    public boolean deleteStudent(Student student) {
        String sql = "DELETE FROM student WHERE name='" + student.getName() + "'";
        PreparedStatement pstmt;

        try {
            pstmt = dbConnector.createConnect().prepareStatement(sql);
            pstmt.execute();
            pstmt.close();
            return true;
        } catch (SQLException e) {
            System.out.println("Delete failed.");
            System.out.println(e.toString());
        }
        return false;
    }
}
